package com.duxcast.smshub.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Settings {

    static SharedPreferences sPref;

    public static void setParam(String param, String value, Context context) {
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
        Editor ed = sPref.edit();
        ed.putString(param, value);
        ed.commit();
    }

    public static String getParam(String param, Context context) {
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sPref.getString(param, "");
    }

    public static String getParam(String param, String defaultParam, Context context) {
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sPref.getString(param, defaultParam);
    }

}