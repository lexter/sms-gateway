package com.duxcast.smshub.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by admin on 01.02.16.
 */
public class RegularExp {

    public static boolean phoneCheck(String userNameString){
        if (userNameString.charAt(0)=='+' && userNameString.charAt(1)=='7') userNameString = userNameString.replaceAll("\\+7","8");
        userNameString = userNameString.replaceAll("-","");
        userNameString = userNameString.replaceAll(" ","");
        if (userNameString.length()!=11) return false;
        return true;
    }

}
