package com.duxcast.smshub.utils;

public class SimpleHeader {
	
	public String key;
	public String value;
	
	public SimpleHeader(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
}