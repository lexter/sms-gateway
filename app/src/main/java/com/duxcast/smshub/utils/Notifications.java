package com.duxcast.smshub.utils;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.duxcast.smshub.activites.MainActivity;
import com.duxcast.smshub.R;

public class Notifications {

    Context context;

    @SuppressLint("NewApi")
    public static Notification generateNotification(Context context) {
        try {
            Intent notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,	notificationIntent, 0);

            try {
                Notification.Builder builder = new Notification.Builder(context)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(context.getString(R.string.server_is_run))
                        .setTicker(context.getString(R.string.server_is_run))
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(R.drawable.ic_action_send_now);
                return builder.getNotification();
            } catch (NoClassDefFoundError e) {
                e.printStackTrace();
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}