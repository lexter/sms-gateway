package com.duxcast.smshub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.duxcast.smshub.R;
import com.duxcast.smshub.models.Sms;

import java.util.List;

public class LogListViewAdapter extends ArrayAdapter<Sms> {


    public LogListViewAdapter(Context context, int resource, List<Sms> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_log,parent,false);

        TextView idTextView = (TextView) convertView.findViewById(R.id.LogIdView);
        idTextView.setText(String.valueOf(getItem(position)._id));

        TextView phoneTextView = (TextView) convertView.findViewById(R.id.LogPhoneView);
        phoneTextView.setText(getItem(position).phone);

        TextView messageTextView = (TextView) convertView.findViewById(R.id.LogMessageView);
        messageTextView.setText(getItem(position).message);

        TextView statusTextView = (TextView) convertView.findViewById(R.id.LogStatusView);
        statusTextView.setText(getItem(position).status);

        return convertView;
    }
}
