package com.duxcast.smshub.broadcastreceivers;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.duxcast.smshub.services.HttpServerService;

import java.util.Date;

public class StartHttpServiceBroadcastReceiver extends BroadcastReceiver {

    final String LOG_TAG = "myLogs";

    public void onReceive(Context context, Intent intent2) {
        Log.i(LOG_TAG, "onReceive " + intent2.getAction());
        Intent intent = new Intent(context, HttpServerService.class);
        PendingIntent mPendingIntent = PendingIntent.getService(context, 1239, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mAlarmManager.cancel(mPendingIntent);
        mAlarmManager.setRepeating(AlarmManager.RTC, new Date().getTime(), 5*60*1000, mPendingIntent);
    }
}