package com.duxcast.smshub.broadcastreceivers;

/**
 * Created by admin on 29.11.15.
 */
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.duxcast.smshub.databases.DbOpenHelper;
import com.duxcast.smshub.models.Sms;

public class DeliverySmsBroadcastReceiver extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        Sms sms = Sms.getSmsById(DbOpenHelper.getInstance(context).getDb(), intent.getIntExtra("message_id", 0));
        if (sms==null) return;
        switch(getResultCode()) {
            case Activity.RESULT_OK:
                Toast.makeText(context, "SMS delivered", Toast.LENGTH_SHORT).show();
                sms.status = Sms.DELIVERED;
                sms.update(DbOpenHelper.getInstance(context).getDb());
                break;
            case Activity.RESULT_CANCELED:
                Toast.makeText(context, "SMS not delivered", Toast.LENGTH_SHORT).show();
                sms.status = Sms.NOT_DELIVERED;
                sms.update(DbOpenHelper.getInstance(context).getDb());
                break;
        }
    }

}