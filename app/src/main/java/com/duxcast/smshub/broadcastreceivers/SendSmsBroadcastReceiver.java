package com.duxcast.smshub.broadcastreceivers;

import android.telephony.SmsManager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.duxcast.smshub.databases.DbOpenHelper;
import com.duxcast.smshub.models.Sms;

public class SendSmsBroadcastReceiver extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        Sms sms = Sms.getSmsById(DbOpenHelper.getInstance(context).getDb(), intent.getIntExtra("message_id", 0));
        if (sms==null) {
            Toast.makeText(context, "SMS not found",Toast.LENGTH_SHORT).show();
            return;
        }
        switch(getResultCode()) {
            case Activity.RESULT_OK:
                Toast.makeText(context, "SMS send",Toast.LENGTH_SHORT).show();
                sms.status = Sms.SENT;
                sms.update(DbOpenHelper.getInstance(context).getDb());
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                Toast.makeText(context, "unknown problems",Toast.LENGTH_SHORT).show();
                sms.status = Sms.ERROR;
                sms.update(DbOpenHelper.getInstance(context).getDb());
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                Toast.makeText(context, "modul is down",Toast.LENGTH_SHORT).show();
                sms.status = Sms.ERROR;
                sms.update(DbOpenHelper.getInstance(context).getDb());
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                Toast.makeText(context, "PDU error",Toast.LENGTH_SHORT).show();
                sms.status = Sms.ERROR;
                sms.update(DbOpenHelper.getInstance(context).getDb());
                break;
        }
    }

}