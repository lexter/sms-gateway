package com.duxcast.smshub.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.duxcast.smshub.models.Client;
import com.duxcast.smshub.utils.Settings;
import com.duxcast.smshub.utils.Notifications;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

public class HttpServerService extends IntentService {

    public HttpServerService() {
        super("HttpServerService");
    }
    Timer timer;

    @Override
    public void onCreate() {
        Toast.makeText(getApplicationContext(), "SERVICE CREATE", Toast.LENGTH_SHORT).show();
        timer = new Timer();
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        startForeground(1203, Notifications.generateNotification(getApplicationContext()));
        try {
            new Client(getApplicationContext()).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
        stopForeground(true);
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "SERVICE DESTROY", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

}
