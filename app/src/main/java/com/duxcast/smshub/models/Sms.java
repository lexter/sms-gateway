package com.duxcast.smshub.models;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.SmsManager;

import com.duxcast.smshub.databases.TimeUtils;

import java.util.ArrayList;
import java.util.Date;

public class Sms {

    public static final String QUEUE = "SMS_QUEUE";
    public static final String SENT = "SMS_SENT";
    public static final String DELIVERED = "SMS_DELIVERED";
    public static final String NOT_DELIVERED = "SMS_NOT_DELIVERED";

    public static final String ERROR = "SMS_ERROR";

    public int _id;
    public int service_id;
    public String phone;
    public String message;
    public String status;
    public Date lastactive;

    public Sms(int service_id,String phone, String message) {
        this.phone = phone;
        this.service_id = service_id;
        this.message = message;
        this.status = QUEUE;
        lastactive = new Date();
    }

    public Sms(Cursor cursor) {
        try {
            _id = cursor.getInt(cursor.getColumnIndex("_id"));
            service_id = cursor.getInt(cursor.getColumnIndex("service_id"));
            phone = cursor.getString(cursor.getColumnIndex("phone"));
            message = cursor.getString(cursor.getColumnIndex("message"));
            status = cursor.getString(cursor.getColumnIndex("status"));
            lastactive = TimeUtils.BDToDate(cursor.getString(cursor.getColumnIndex("lastactive")));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void save(SQLiteDatabase db) {
        try {
            ContentValues insertValues = new ContentValues();
            insertValues.put("service_id", service_id);
            insertValues.put("phone", phone);
            insertValues.put("message", message);
            insertValues.put("status", status);
            insertValues.put("lastactive", TimeUtils.DateToBD(lastactive));
            _id = (int) db.insert("sms", null, insertValues);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void update(SQLiteDatabase db) {
        try {
            if (_id==0) return;
            ContentValues insertValues = new ContentValues();
            insertValues.put("service_id", service_id);
            insertValues.put("phone", phone);
            insertValues.put("message", message);
            insertValues.put("status", status);
            lastactive = new Date();
            insertValues.put("lastactive", TimeUtils.DateToBD(lastactive));
            db.update("sms", insertValues, "_id=" + _id, null);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public boolean remove(SQLiteDatabase db) {
        try {
            db.execSQL("DELETE FROM 'sms' WHERE _id = " + _id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean removeAll(SQLiteDatabase db,String table) {
        try {
            db.execSQL("DELETE FROM "+table+" WHERE 1");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Sms getSmsById(SQLiteDatabase db, int _id) {
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM 'sms' WHERE _id = "+_id, null);
            if (cursor.moveToFirst()) {
                Sms sms = new Sms(cursor);
                return sms;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }

    public static Sms getSmsByServiceId(SQLiteDatabase db, int service_id) {
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM 'sms' WHERE service_id = "+service_id, null);
            if (cursor.moveToFirst()) {
                Sms sms = new Sms(cursor);
                return sms;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }

    public static ArrayList<Sms> getAllSms(SQLiteDatabase db) {
        ArrayList<Sms> smses = new ArrayList<Sms>();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM 'sms' WHERE 1 ORDER BY lastactive DESC LIMIT 0,200", null);
            while (cursor.moveToNext()) {
                smses.add(new Sms(cursor));
            }
            return smses;
        } catch (Exception e) {
            e.printStackTrace();
            return smses;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }

    public static ArrayList<Sms> getAllForResendSms(SQLiteDatabase db) {
        ArrayList<Sms> smses = new ArrayList<Sms>();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM 'sms' WHERE status!='SMS_DELIVERED' ORDER BY lastactive DESC LIMIT 0,500", null);
            while (cursor.moveToNext()) {
                smses.add(new Sms(cursor));
            }
            return smses;
        } catch (Exception e) {
            e.printStackTrace();
            return smses;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }

    public boolean send(SmsManager smsManager,Context context) {

        Intent intentSent = new Intent(SENT);
        intentSent.putExtra("message_id",_id);
        PendingIntent sentPI = PendingIntent.getBroadcast(context, _id, intentSent, 0);

        Intent intentDelivered = new Intent(DELIVERED);
        intentDelivered.putExtra("message_id",_id);
        PendingIntent delivertPI = PendingIntent.getBroadcast(context, _id, intentDelivered, 0);

        ArrayList<String> mArray = smsManager.divideMessage(message);

        ArrayList<PendingIntent> sentArrayIntents = new ArrayList<PendingIntent>();
        ArrayList<PendingIntent> delivertArrayIntents = new ArrayList<PendingIntent>();

        for (int i = 0; i < mArray.size(); i++) {
            sentArrayIntents.add(sentPI);
            delivertArrayIntents.add(delivertPI);
        }

        smsManager.sendMultipartTextMessage(phone, null, mArray, sentArrayIntents, delivertArrayIntents);

        return true;
    }

}
