package com.duxcast.smshub.models;

import android.content.Context;
import android.telephony.SmsManager;

import com.duxcast.smshub.databases.DbOpenHelper;
import com.duxcast.smshub.utils.HttpRequest;
import com.duxcast.smshub.utils.RegularExp;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class Client implements Runnable {

	Context context;

	public Client(Context context) {
		this.context = context;
	}

	public void run() {
		try {
			handleClient();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void handleClient() throws IOException {

		try {
			String url = "http://URL.ru";

			JSONObject statusSmsesObject = new JSONObject();
			ArrayList<Sms> sendSmses = Sms.getAllSms(DbOpenHelper.getInstance(context).getDb());
			JSONArray array = new JSONArray();
			for (Sms sms:sendSmses) {
				JSONObject object = new JSONObject();
				object.put("status",sms.status);
				object.put("service_id",sms.service_id);
				object.put("gateway_id", sms._id);
				array.put(object);
			}

			statusSmsesObject.put("statussmses", array);

			//request!

			String result = HttpRequest.execute(false, url, statusSmsesObject.toString(), null);
			JSONObject jsonObject = new JSONObject(result);
			JSONArray smsreturnstatus = jsonObject.optJSONArray("smsreturnstatus");
			for (int i=0;i<smsreturnstatus.length();i++) {
				JSONObject sms = smsreturnstatus.getJSONObject(i);
				if (sms.optString("status").equals("ok")) {
					System.out.println("removed");
					Sms smsObj = Sms.getSmsByServiceId(DbOpenHelper.getInstance(context).getDb(), sms.optInt("service_id"));
					if (smsObj!=null) smsObj.remove(DbOpenHelper.getInstance(context).getDb());
				}
			}

			JSONArray smsforsend = jsonObject.optJSONArray("smsforsend");
			for (int i=0;i<smsforsend.length();i++) {
				JSONObject objectSms = smsforsend.getJSONObject(i);
				if (RegularExp.phoneCheck(objectSms.optString("phone"))) {
					Sms sms = new Sms(objectSms.optInt("service_id"),objectSms.optString("phone"), objectSms.optString("message"));
					sms.save(DbOpenHelper.getInstance(context).getDb());
					sendSmses.add(sms);
				}
			}

			ArrayList<Sms> forSendSmses = Sms.getAllSms(DbOpenHelper.getInstance(context).getDb());
			for (Sms sms:forSendSmses) {
				if (sms.status.equals("SMS_QUEUE")) {
					sms.send(SmsManager.getDefault(), context);
					try {
						Thread.sleep(5000);
					} catch (Exception ignore) {
						//ignore
					}
				}
			}


		} catch (Exception e) {
			System.out.println("error !!! ");
			e.printStackTrace();
		}


	}


}