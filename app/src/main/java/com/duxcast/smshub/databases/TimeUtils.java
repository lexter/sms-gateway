package com.duxcast.smshub.databases;

/**
 * Created by admin on 29.01.16.
 */
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;

public class TimeUtils {

    public static String DateToBD(Date date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
            return dateFormat.format(date);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date BDToDate(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
            return dateFormat.parse(date);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date BDToDate2(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.parse(date);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static Date BDToDateWithoutSec(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            //dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.parse(date);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date time(String date1) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            //dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return getDateInTimeZone(dateFormat.parse(date1));
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date timeWithoutSec(String date1) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            //dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.parse(date1);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getDateInTimeZone(Date currentDate) {
        //TimeZone tz = TimeZone.getTimeZone(timeZoneId);
        Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        mbCal.setTimeInMillis(currentDate.getTime());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
        cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
        cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
        cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));
        return cal.getTime();
    }


}