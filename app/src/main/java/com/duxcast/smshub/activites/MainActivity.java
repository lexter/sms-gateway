package com.duxcast.smshub.activites;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.duxcast.smshub.R;
import com.duxcast.smshub.adapters.LogListViewAdapter;
import com.duxcast.smshub.databases.DbOpenHelper;
import com.duxcast.smshub.models.Sms;
import com.duxcast.smshub.services.HttpServerService;

import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends ActionBarActivity {

    public String TAG = "MainActivity";

    ListView listView;
    LogListViewAdapter logListViewAdapter;
    BroadcastReceiver br;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Intent intent = new Intent(this, HttpServerService.class);
        PendingIntent mPendingIntent = PendingIntent.getService(this, 1239, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mAlarmManager.cancel(mPendingIntent);
        mAlarmManager.setRepeating(AlarmManager.RTC, new Date().getTime(), 120 * 1000, mPendingIntent);

        listView = (ListView) findViewById(R.id.listViewLogSms);
        ArrayList<Sms> smses = Sms.getAllSms(DbOpenHelper.getInstance(getApplicationContext()).getDb());
        logListViewAdapter = new LogListViewAdapter(getApplicationContext(),R.layout.item_log,smses);
        listView.setAdapter(logListViewAdapter);

        br = new BroadcastReceiver() {
            // действия при получении сообщений
            public void onReceive(Context context, Intent intent) {
                updateListView();
            }
        };

        // регистрируем (включаем) BroadcastReceiver
        registerReceiver(br, new IntentFilter("SMS_DELIVERED"));
        registerReceiver(br, new IntentFilter("SMS_SENT"));

    }

    void updateListView() {
        if (listView==null || logListViewAdapter==null) return;
        logListViewAdapter.clear();
        logListViewAdapter.addAll(Sms.getAllSms(DbOpenHelper.getInstance(getApplicationContext()).getDb()));
        logListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                updateListView();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(br);
        super.onDestroy();
    }


}
